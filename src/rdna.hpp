#include <string>
#include <random>

using std::string;

string randDNA(int seed, string bases, int n){
	std::mt19937 engine1(seed);
	
	float mean = 37.5f;
	float sd = 3.5f;
	
	std::normal_distribution<float> norm(mean,sd);
	
	for(int i = 0; i < n; i++){
		bases+= norm(engine1);
		bases+= " ";
	}
	
	 return bases;
}
